#include "common.h" 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

// 定義狀態
#define STATE_LIGHT_OFF 1      // 關燈
#define STATE_LIGHT_ON 2       // 開燈
#define STATE_LIGHT_DIMMING 3  // 燈閃爍

// 定義事件
#define EVENT_ON 101           // 觸發「ON」
#define EVENT_OFF 102          // 觸發「OFF」
#define EVENT_DIMMING 103      // 觸發「Dimming」
#define EVENT_TIMEOUT 104      // 觸發「Timeout」

#define TICK_COUNT 50          // timeout為5秒, 每次進入回圈為1/10秒，50次就是5秒了
#define TICK_DISABLE -1        // 當不使用timer時，設定為-1

/*..........................................................................*/
// 函式宣告
void ShowState(int current_state);
void light_on();
void light_off();
void light_dimming();

/*..........................................................................*/
// 全域變數
int current_state;  // 目前的狀態
int tick;           // 目前的tick數

void light_state_machine_dispatch(int new_event)
{
  if( current_state == STATE_LIGHT_OFF )
  {
    // 在OFF的狀態下，只有TIMEOUT跟ON這兩個Event觸發，才進行處理

     if( new_event == EVENT_TIMEOUT )
    {
      light_on();
      tick = TICK_DISABLE;
      current_state = STATE_LIGHT_ON;
    }
    else if( new_event == EVENT_ON )
    { 
      light_on();
      tick = TICK_DISABLE;
      current_state = STATE_LIGHT_ON;
    }

  }// end of if( current_state == LIGHT_OFF )
  else if( current_state == STATE_LIGHT_ON )
  {
    // 在OFF的狀態下，只有OFF跟DIMMING這兩個Event觸發，才進行處理

    if( new_event == EVENT_OFF )
    {
      light_off();
      tick = TICK_COUNT;
      current_state = STATE_LIGHT_OFF;
    }
    else if( new_event == EVENT_DIMMING )
    {
      light_dimming();
      tick = TICK_DISABLE;
      current_state = STATE_LIGHT_DIMMING;
    }

  }// end of else if( current_state == LIGHT_ON )
  else if( current_state == STATE_LIGHT_DIMMING )
  {
    // 在OFF的狀態下，只有OFF Event觸發，才進行處理

    if( new_event == EVENT_OFF)
    {
      light_off();
      tick = TICK_COUNT;
      current_state = STATE_LIGHT_OFF;
    }
    
  }// end of else if( current_state == LIGHT_DIMMING )
}

/*..........................................................................*/
int main() {

  // 初始狀態設定為OFF，tick數為50
  current_state = STATE_LIGHT_OFF; 
  tick = TICK_COUNT;         
  
  // 提示可輸入控制的字元
  printf("Stand Lamp\n"
         "Press 'o'   for push on button\n"
         "Press 'f'   for push off button\n"
         "Press 'd'   for push dimming button\n"
         "Press <Esc> to exit.\n");
  
  // 先秀出目前的狀態 
  ShowState(current_state); 
  
  // 主要的處理迴圈
  for (;;) {
      
      // 每1/10秒處理一次
      usleep(100000);
      
      // 如果timer是啟動的，tick則是每次遞減
      if( tick >= 0 )
      {
        tick--;
      }
      
      // tick已經由50次遞減至0，則發出EVENT_TIMEOUT
      if (tick == 0) {
          light_state_machine_dispatch(EVENT_TIMEOUT);
          ShowState(current_state);
      }

      // 檢查 User是不是有輸入任何的字元
      if (__kbhit()) {
          switch (__getch()) {
              case 'o': { // User按了ON
                  light_state_machine_dispatch(EVENT_ON);
                  break;
              }

              case 'f': { // User按了OFF
                  light_state_machine_dispatch(EVENT_OFF);
                  break;
              }
              
              case 'd': { // User按了Dimming
                  light_state_machine_dispatch(EVENT_DIMMING);
                  break;
              }
              case '\33': { // User按了ESC
                  printf("\nESC : exit");
                  _exit(0);
                  break;
              }
          }
          ShowState(current_state);
      }
  }    
  return 0;
}

/*..........................................................................*/

// 秀出目前的狀態
void ShowState(int current_state)
{
  if(current_state==STATE_LIGHT_ON)
  {
    printf("[LIGHT_ON]\n");
  }
  else if(current_state==STATE_LIGHT_OFF)
  {
    printf("[LIGHT_OFF]\n");
  }
  else if(current_state==STATE_LIGHT_DIMMING)
  {
    printf("[LIGHT_DIMMING]\n");
  }    
}

// Light on發生時要執行的功能
void light_on()
{
  printf("*Light On\n");
}

// Light off發生時要執行的功能
void light_off()
{
  printf("*Light Off\n");
}

// Light dimming發生時要執行的功能
void light_dimming()
{
  printf("*Light Dimming\n");
}