#include "common.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

// 定義轉態的巨集，除了把新狀態存下來至state,順便會秀出轉態
#define TRAN(_target)  (me->state = (_target)); ShowState(me);

// 定義 TIMEOUT為5秒
#define TIMEOUT 5

/*..........................................................................*/
// 定義

// 定義訊息種類
typedef enum {
  MSG_PUSH_BUTTON,                  // 按鈕的訊息
  MSG_TICK,                         // Tick的訊息
  MSG_DETECT_STUFF,                 // 偵測到有東西訊息
  MSG_DETECT_NOTHING,               // 沒有偵測到的訊息
  MSG_SECURITY_BUTTON,              // Security按鈕的訊息
  MSG_CANCEL_BUTTON,                // Cancel按鈕的訊息
  MSG_ANY_NUM                       // 數字鍵的訊息
}MSG_TYPE;

// 定義狀態
typedef enum {
  STATE_CLOSED,                     // 關閉的狀態
  STATE_OPENED,                     // 開啟的狀態
  STATE_KEEP_OPEN,                  // 保持開啟的狀態
  STATE_PASSWORD_ENTERING,          // 等待輸入密碼的狀態
  STATE_SECURITY,                   // 保全的狀態
  STATE_VERIFY,                     // 驗證密碼的狀態
  STATE_BEEP,                       // 驗證錯誤的狀態
  STATE_ALARM                       // 三次驗證錯誤，警告的狀態
}STATE;

// 定義事件的結構，目前只有含一個訊息的種類ID
typedef struct EventTag {
  uint8_t msg;                      // 訊息種類
} Event;

// 定義Tick事件的結構
typedef struct TickEventTag {
  Event super;                      // 事件
  uint8_t times;                    // 事件延伸的屬性，這邊是「次數」的累計
} TickEvent;

// 定義數字字元輸入事件的結構
typedef struct NumEventTag {
  Event super;                      // 事件
  uint8_t num;                      // 事件延伸的屬性，這邊放的是「數字」的字元
} NumEvent;


// 儲存狀態機所有變數的結構
typedef struct AutoDoor3Tag {
  STATE state;                      // 目前狀態機的狀態
  uint8_t timeout;                  // 目前距離timeout的秒數
  char password[5];                 // 要設定的密碼，4位數字，第5個字元為結尾字元
  uint8_t password_pos;             // 要設定的密碼，目前設定到第幾個字元
  char verify_password[5];          // 要驗證的密碼，4位數字，第5個字元為結尾字元
  uint8_t verify_password_pos;      // 要驗證的密碼，目前設定到第幾個字元
  uint8_t verify_fail_times;        // 驗證錯誤的次數
} AutoDoor3;

/*..........................................................................*/
// 函式宣告

// 初始化狀態機的所有相關變數
void AutoDoor3_init(AutoDoor3 *me);

// 狀態機的分派事件，轉態的工作
void AutoDoor3_dispatch(AutoDoor3 *me, Event const *e);

// 秀出目前的事件
void ShowState(AutoDoor3 *me);


/*..........................................................................*/
// 全域變數

// 儲存自動門狀態機所有變數的宣告
static AutoDoor3 g_autodoor3; 

/*..........................................................................*/
void AutoDoor3_init(AutoDoor3 *me) {

  // 設定timeout
  me->timeout = TIMEOUT;

  // 初始化設定的密碼
  me->password_pos = 0;
  me->password[0]='\0';

  // 初始化驗證的密碼
  me->verify_password_pos = 0;
  me->verify_password[0]='\0';

  // 初始化「驗證錯誤計數器」
  me->verify_fail_times = 0;

  // 目前狀態為Closed  
  TRAN(STATE_CLOSED);
}
/*..........................................................................*/
void AutoDoor3_dispatch(AutoDoor3 *me, Event const *e) {
  switch (me->state) {
      // 在Closed的狀態下只處理 Push Button, Security Button 的訊息
      case STATE_CLOSED: {
          switch (e->msg) {
              case MSG_PUSH_BUTTON: {
                  // 設定 time out
                  me->timeout=TIMEOUT; 
                  // 開門
                  printf("*door opening\n");
                  // 轉態至開門
                  TRAN(STATE_OPENED);
                  break;
              }
              
              case MSG_SECURITY_BUTTON: {
                  // 提示
                  printf("enter 4 numbers password to set security mode\n");
                  // 重置
                  me->password_pos = 0;
                  // 轉態至設定密碼的狀態
                  TRAN(STATE_PASSWORD_ENTERING);
                  break;
              }
                
              
          }
          break;
      }
      case STATE_OPENED: {
          switch (e->msg) {
              // 在Opened的狀態下只處理 Push Button, Tick, Detect stuff 這三種訊息
              case MSG_PUSH_BUTTON: {
                  // 關門
                  printf("*door closing\n");
                  // 轉態至關門
                  TRAN(STATE_CLOSED);
                  break;
              }
              case MSG_TICK: {
                  // 每1/10秒，times會從0~9的變化。所以這個地方每1/10秒會進來一次
                  
                  if (((TickEvent*)e)->times == 0) { //只在times為0的時候進來
                      
                      // 這個地方一秒會進來一次
                      // 印出距離timeout的秒數
                      printf("After %d seconds, door will auto closing\n", 
                             me->timeout);
                      
                      // 把timeout減一
                      --me->timeout;
                      
                      // 檢查 timeout是否到了
                      if (me->timeout == 0 ) {
                          
                          // 關門
                          printf("*timeout : door auto closing\n");
                          
                          // 轉態至關門
                          TRAN(STATE_CLOSED);
                          break;
                      }
                  }
                  break;
              }
              case MSG_DETECT_STUFF: {
                  // 電動門機電保持門在開啟
                  printf("*door keep opening\n");
                  
                  // 轉態至開啟的狀態
                  TRAN(STATE_KEEP_OPEN);
                  break;
              }

          }
          break;
      }
      
      case STATE_KEEP_OPEN:{
          // 在Keep Open的狀態下只處理 detect nothing 的訊息
          switch (e->msg) {
              case MSG_DETECT_NOTHING: {
                  
                  // 電動門將在5秒後關閉
                  printf("*door will automatic close\n");
                  
                  // 設定timeout
                  me->timeout=TIMEOUT;
                  
                  // 轉態至Opened
                  TRAN(STATE_OPENED);
                  break;
              }
          }
          break;
      }
      
      case STATE_PASSWORD_ENTERING:{
        // 在輸入密碼的狀態下，只處理cancel button,跟數字鍵的輸入訊息
        switch (e->msg) {
            case MSG_CANCEL_BUTTON: {
                // 取消設定密碼
                printf("cancel input password\n");
                TRAN(STATE_CLOSED);
                break;
            }
            case MSG_ANY_NUM: {
                // 將輸入的設定密碼儲存下來
                me->password[ me->password_pos ] = ((NumEvent*)e)->num;
                me->password_pos++; 
                me->password[ me->password_pos ] = '\0';
                
                // 假如密碼已經4位數了，那就轉態至保全模式了
                if( me->password_pos==4 )
                {
                    printf("\nset password complete\n");
                    TRAN(STATE_SECURITY);
                    break;
                }
                else
                {
                    // keep in STATE_PASSWORD_ENTERING
                    break;  
                }  
                break;
            }

        }    
        break;     
      }
     
      case STATE_SECURITY:{
        // 在保全模式的狀態下只處理 cancel button 的訊息
        switch (e->msg) {
            case MSG_CANCEL_BUTTON: {
                // 提示要離開保全模式
                printf("enter 4 numbers password to cancel security mode\n");
                // 轉態至驗證狀態
                TRAN(STATE_VERIFY);
                // 重置驗證所需要的資訊
                me->verify_fail_times = 0;
                me->verify_password_pos = 0;
                break;
            }
        }    
        break;     
      }
     
      case STATE_VERIFY:{
        // 在驗證模式的狀態下只處理，只處理cancel button,跟數字鍵的輸入訊息
        switch (e->msg) {
            case MSG_ANY_NUM: {
                // 將輸入驗證的密碼存下來
                me->verify_password[ me->verify_password_pos ] = 
                    ((NumEvent*)e)->num;
                me->verify_password_pos++; 
                me->verify_password[ me->verify_password_pos ] = '\0';
                // 當輸入4位數的時候，才做真正驗證
                if( me->verify_password_pos==4 )
                {
                    bool isPass=true;
                    int i;
                    for( i=0; i<4; i++)
                    {
                      // 比較4位數密碼，只要有任何一位數不一樣，就是驗證不過，
                      if( me->verify_password[i]!=me->password[i] )
                      {
                        isPass = false;
                        
                        // 驗證錯誤次數累加
                        me->verify_fail_times++;
                        break;
                      }
                    }
                    
                    // 驗證正確，開門，設定自動關門的timeout, 轉態至開門
                    if( isPass )
                    {
                      printf("\n*password is correct, door is opening\n");
                      me->timeout=TIMEOUT;
                      TRAN(STATE_OPENED);
                      break;
                    }
                    else
                    { //驗證錯誤時，轉態至Beep狀態
                      TRAN(STATE_BEEP);
                      break;
                    }
                }
                break;
            }
            
            case MSG_CANCEL_BUTTON: {
                // 在驗證過程中，按下cancel button,可以取消驗證模式，回到保全模式
                printf("cancel all input, back to Security mode \n");
                TRAN(STATE_SECURITY);
                break;
            }
        }
        break;
      }
      
      case STATE_BEEP:{
        // 在Beep的狀態下，不處理任何的訊息, 直接處理Guard跟轉態
        
        // 重置驗證密碼相關的變數
        me->verify_password_pos=0;
        
        printf("*beep : worng password!\n");
        
        // Guard
        if( me->verify_fail_times==3 )
        {
          // 錯誤三次，轉態至Alarm狀態
          TRAN(STATE_ALARM);
          // 設定timeout 6秒
          me->timeout=6;
          // 通知警衛
          printf("*alarm : notify guard\n");
          break;
        }else
        {
          // 錯誤低於三次，仍然回到驗證狀態，繼續驗證
          TRAN(STATE_VERIFY);
          break;
        }     
      }
       
      case STATE_ALARM:{
        // 在Alarm的狀態下，只處理tick的訊息
        switch (e->msg) {
            case MSG_TICK: {
                // 每1/10秒，times會從0~9的變化。所以這個地方每1/10秒會進來一次
                if (((TickEvent*)e)->times == 0) { //只在times為0的時候進來
                    // 這個地方一秒會進來一次
                    
                    // 印出距離timeout的秒數
                     printf("After %d second, you can use input function\n",
                             me->timeout);
                    // time out秒數減1秒
                    --me->timeout;
                    // 檢查time out是否由6秒遞減至0
                    if (me->timeout == 0) {
                      // 經過6秒了，回到驗證的狀態，可以繼續驗證了
                      printf("you can retry now\n");
                      TRAN(STATE_VERIFY);
                      break;
                    }
                }
                break;
            }
        }
        break;
      }
  }
}



/*..........................................................................*/
int main() {

  // 提示可輸入控制的字元
  printf("autodoor3\n"
         "Press 'p'   for push button\n"
         "Press 'i'   for stuff in door\n"
         "Press 'o'   for stuff out door\n"
         "Press 'c'   for cancel button\n"
         "Press 's'   for security the door\n"
         "Press '0~9' for password number\n"
         "Press <Esc> to exit.\n");

  // 初始化狀態機
  AutoDoor3_init(&g_autodoor3);

  // 主要的處理迴圈
  for (;;) {
       // 生成Tick Event
      static TickEvent evt_tick = { MSG_TICK, 0};
  
      // 每1/10秒處理一次
      usleep(100000);
  
      // 設定累加Tick Event的times(0~9之間)
      if (++evt_tick.times == 10) {
          evt_tick.times = 0;
      }

      // 分派tick event
      AutoDoor3_dispatch(&g_autodoor3, (Event *)&evt_tick);

      // 檢查 User是不是有輸入任何的字元
      if (__kbhit()) {

        // 生成Button Event
        static Event const evt_push_btn   = { MSG_PUSH_BUTTON   };
        // 生成 Detect Stuff Event
        static Event const evt_detect_stuff   = { MSG_DETECT_STUFF   };
        // 生成 Detect Nothing Event
        static Event const evt_detecct_nothing   = { MSG_DETECT_NOTHING   };
        // 生成 Cancel Button Event
        static Event const evt_cancel_btn   = { MSG_CANCEL_BUTTON   };
        // 生成 Security Button Event.
        static Event const evt_security_btn   = { MSG_SECURITY_BUTTON   };

        Event const *e = (Event *)0;
        uint8_t ch;

        switch (ch = __getch()) {
            case 'p': { // User按了push button
                printf("push button : ");
                e = &evt_push_btn;
                break;
            }
            
            case 'i': { // 模擬感測器，感測到有人或是物品
                printf("detect stuff : ");
                e = &evt_detect_stuff;
                break;
            }
            
            case 'o': { // 模擬感測器，沒有偵測到人跟物品
                printf("detect nothing : ");
                e = &evt_detecct_nothing;
                break;
            }
            
            case 's': { // User按了Security Button
                printf("security button : ");
                e = &evt_security_btn;
                break;
            }
            
            case 'c': { // User按了Cancel Button
                printf("cancel button : ");
                e = &evt_cancel_btn;
                break;
            }
            
            // User按了數字鍵
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
            {
              static NumEvent evt_num = { MSG_ANY_NUM, 0};
              evt_num.num = ch; // 將所按的數字置入NumEvent中

              // 分派
              AutoDoor3_dispatch(&g_autodoor3, (Event *)&evt_num);
              
              continue;
            }

            case '\33': {
                printf("ESC : exit");
                _exit(0);
                break;
            }
        }

        if (e != (Event *)0) { // 假如 e有設定，那就分派它
            AutoDoor3_dispatch(&g_autodoor3, e);
        }
      }
  }
  
  return 0;
}

// 秀出目前的狀態
void ShowState(AutoDoor3 *me)
{
  switch(me->state)
  {
    case STATE_CLOSED:
      printf("[CLOSED]\n");
    break;
    
    case STATE_OPENED:
      printf("[OPENED]\n");
    break;
    
    case STATE_KEEP_OPEN:
      printf("[KEEP OPENED] Detect stuff\n");
    break;
    
    case STATE_PASSWORD_ENTERING:
      printf("[PASSWORD_ENTERING]\n");
    break;
          
    case STATE_SECURITY:
      printf("[SECURITY]\n");
    break;
    
    case STATE_VERIFY:
      printf("[VERIFY]\n");
    break;    
    
    case STATE_BEEP:
      printf("[BEEP]\n");
    break;
        
    case STATE_ALARM:
      printf("[ALARM]\n");
    break;
    
  }
}
