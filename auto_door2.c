#include "common.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

// 定義轉態的巨集，除了把新狀態存下來至state,順便會秀出轉態
#define TRAN(_target)  (me->state = (_target)); ShowState(me);

// 定義 TIMEOUT為5秒
#define TIMEOUT 5

/*..........................................................................*/
// 定義

// 定義訊息種類
typedef enum {
  MSG_PUSH_BUTTON,                  // 按鈕的訊息
  MSG_TICK,                         // Tick的訊息
  MSG_DETECT_STUFF,                 // 偵測到有東西訊息
  MSG_DETECT_NOTHING                // 沒有偵測到的訊息
}MSG_TYPE;

// 定義狀態
typedef enum {
  STATE_CLOSED,                     // 關閉的狀態
  STATE_OPENED,                     // 開啟的狀態
  STATE_KEEP_OPEN                   // 保持開啟的狀態
}STATE;

// 定義事件的結構，目前只有含一個訊息的種類ID
typedef struct EventTag {
  uint8_t msg;                      // 訊息種類
} Event;

// 定義Tick事件的結構
typedef struct TickEventTag {
  Event super;                      // 事件
  uint8_t times;                    // 事件延伸的屬性，這邊是「次數」的累計
} TickEvent;

// 儲存狀態機所有變數的結構
typedef struct AutoDoor2Tag {
  STATE state;                      // 目前狀態機的狀態
  uint8_t timeout;                  // 目前距離timeout的秒數
} AutoDoor2;

/*..........................................................................*/
// 函式宣告

// 初始化狀態機的所有相關變數
void AutoDoor2_init(AutoDoor2 *me);

// 狀態機的分派事件，轉態的工作
void AutoDoor2_dispatch(AutoDoor2 *me, Event const *e);

// 秀出目前的事件
void ShowState(AutoDoor2 *me);



/*..........................................................................*/
// 全域變數

// 儲存自動門狀態機所有變數的宣告
static AutoDoor2 g_autodoor2; 


/*..........................................................................*/
void AutoDoor2_init(AutoDoor2 *me) {
  
  // 設定timeout
  me->timeout = TIMEOUT;
  
  // 目前狀態為Closed
  TRAN(STATE_CLOSED);
}
/*..........................................................................*/
void AutoDoor2_dispatch(AutoDoor2 *me, Event const *e) {
  switch (me->state) {
      case STATE_CLOSED: {
          // 在Closed的狀態下只處理 Push Button 的訊息
          switch (e->msg) {
              case MSG_PUSH_BUTTON: {
                  // 設定 time out
                  me->timeout=TIMEOUT; 
                  // 開門
                  printf("*door opening\n");
                  // 轉態至開門
                  TRAN(STATE_OPENED);
                  break;
              }
              
          }
          break;
      }
      case STATE_OPENED: {
      // 在Opened的狀態下只處理 Push Button, Tick, detect stuff 這三種訊息
          switch (e->msg) {
              case MSG_PUSH_BUTTON: {
                  // 關門
                  printf("*door closing\n");
                  // 轉態至關門
                  TRAN(STATE_CLOSED);
                  break;
              }
              case MSG_TICK: {
                  // 每1/10秒，times會從0~9的變化。所以這個地方每1/10秒會進來一次
                  
                  if (((TickEvent*)e)->times == 0) { //只在times為0的時候進來
                      
                      // 這個地方一秒會進來一次
                      
                      // 印出距離timeout的秒數
                      printf("After %d seconds, door will auto closing\n",
                              me->timeout);
                      
                      // 把timeout減一
                      --me->timeout;
                      
                      // 檢查 timeout是否到了
                      if (me->timeout == 0 ) {
                          
                          // 關門
                          printf("*timeout : door auto closing\n");
                          
                          // 轉態至關門
                          TRAN(STATE_CLOSED);
                          break;
                      }
                  }
                  break;
              }
              case MSG_DETECT_STUFF: {
                  // 電動門機電保持門在開啟
                  printf("*door keep opening\n");
                  
                  // 轉態至開啟的狀態
                  TRAN(STATE_KEEP_OPEN);
                  break;
              }

          }
          break;
      }
      
      case STATE_KEEP_OPEN:{
          // 在Keep Open的狀態下只處理 detect nothing 的訊息
          switch (e->msg) {
              case MSG_DETECT_NOTHING: {
                  
                  // 電動門將在5秒後關閉
                  printf("*door will automatic close\n");
                  
                  // 設定timeout
                  me->timeout=TIMEOUT;
                  
                  // 轉態至Opened
                  TRAN(STATE_OPENED);
                  break;
              }
          }
          break;
      }
  }
}



/*..........................................................................*/
int main() {  

  // 提示可輸入控制的字元
  printf("autodoor2\n"
         "Press 'p'   for push button\n"
         "Press 'i'   for stuff in door\n"
         "Press 'o'   for stuff out door\n"
         "Press <Esc> to exit.\n");


  // 初始化狀態機
  AutoDoor2_init(&g_autodoor2);

  // 主要的處理迴圈
  for (;;) {
       // 生成Tick Event
      static TickEvent evt_tick = { MSG_TICK, 0};
  
      // 每1/10秒處理一次
      usleep(100000);
  
      // 設定累加Tick Event的times(0~9之間)
      if (++evt_tick.times == 10) {
          evt_tick.times = 0;
      }

      // 分派tick event
      AutoDoor2_dispatch(&g_autodoor2, (Event *)&evt_tick);

    // 檢查 User是不是有輸入任何的字元
    if (__kbhit()) {

        // 生成Button Event
        static Event const evt_push_btn   = { MSG_PUSH_BUTTON   };
        // 生成 Detect Stuff Event
        static Event const evt_detect_stuff   = { MSG_DETECT_STUFF   };
        // 生成 Detect Nothing Event
        static Event const evt_detecct_nothing   = { MSG_DETECT_NOTHING   };

        Event const *e = (Event *)0;

        switch (__getch()) {
            case 'p': { // User按了push button
                printf("push button : ");
                e = &evt_push_btn;
                break;
            }
            
            case 'i': { // 模擬感測器，感測到有人或是物品
                printf("detect stuff : ");
                e = &evt_detect_stuff;
                break;
            }
            
            case 'o': { // 模擬感測器，沒有偵測到人跟物品
                printf("detect nothing : ");
                e = &evt_detecct_nothing;
                break;
            }
            
            case '\33': {
                printf("ESC : exit");
                _exit(0);
                break;
            }
        }

        if (e != (Event *)0) { // 假如 e有設定，那就分派它
            AutoDoor2_dispatch(&g_autodoor2, e);
        }
    }
  }

  return 0;
}


// 秀出目前的狀態
void ShowState(AutoDoor2 *me)
{
  switch(me->state)
  {
    case STATE_CLOSED:
      printf("[CLOSED]\n");
    break;
    
    case STATE_OPENED:
      printf("[OPENED]\n");
    break;
    
    case STATE_KEEP_OPEN:
      printf("[KEEP OPEN]\n");
    break;
       
  }  
}
