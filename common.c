#ifdef __MINGW32__ 
#include <conio.h>
#else
#include <termios.h>
#endif

#include <stdio.h>
#include <stdlib.h>                                          
#include <unistd.h>
#include <fcntl.h>


/*..........................................................................*/
int __getch(void)
{
#ifdef __MINGW32__ 
    char ch = getch();
    printf("%c\n",ch);
    return ch;
#else	
    struct termios oldattr, newattr;
    int ch;
    tcgetattr( STDIN_FILENO, &oldattr );
    newattr = oldattr;
    newattr.c_lflag &= ~( ICANON | ECHO );
    tcsetattr( STDIN_FILENO, TCSANOW, &newattr );
    ch = getchar();
    tcsetattr( STDIN_FILENO, TCSANOW, &oldattr );
    printf("\n");
    return ch;
#endif    
}

/*..........................................................................*/
int __kbhit(void)
{
#ifdef __MINGW32__ 
  return kbhit();
#else	
  struct termios oldt, newt;
  int ch;
  int oldf;
 
  tcgetattr(STDIN_FILENO, &oldt);
  newt = oldt;
  newt.c_lflag &= ~(ICANON | ECHO);
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);
  oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
  fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);
 
  ch = getchar();
 
  tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
  fcntl(STDIN_FILENO, F_SETFL, oldf);
 
  if(ch != EOF)
  {
    ungetc(ch, stdin);
    return 1;
  }
 
  return 0;
#endif  
}
